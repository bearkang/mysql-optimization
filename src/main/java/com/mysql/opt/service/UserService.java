package com.mysql.opt.service;

import com.mysql.opt.entity.User;
import com.mysql.opt.entity.UserTest;

import java.util.List;

public interface UserService {
    List<User> getUserList();

    List<User> searchList(String keyword);

    boolean addUsers(List<User> users);

    boolean updateById(User user);

    List<UserTest> getUserTest(Integer id);
}
