package com.mysql.opt.service;

import com.mysql.opt.entity.Address;

import java.util.List;

public interface AddressService {

    boolean addAddress(List<Address> addresses);
}
