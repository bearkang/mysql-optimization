package com.mysql.opt.service.impl;

import com.mysql.opt.dao.UserDao;
import com.mysql.opt.entity.User;
import com.mysql.opt.entity.UserTest;
import com.mysql.opt.service.UserService;
import org.ehcache.core.util.CollectionUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public List<User> getUserList() {
        return userDao.getUserList();
    }

    @Override
    public List<User> searchList(String keyword) {
        return userDao.searchList(keyword);
    }


    @Override
    public boolean addUsers(List<User> users) {
        return userDao.addUsers(users);
    }

    @Override
    public boolean updateById(User user) {
        Integer b = userDao.updateById(user);
        return false;
    }

    @Override
    public List<UserTest> getUserTest(Integer id) {
        List<UserTest> userTest = userDao.getUserTest(id);
        if (userTest != null && userTest.size() > 0) {
            UserTest test = userTest.get(0);
            String bizData = test.getUserName(); // null or exception？
            System.out.println(bizData);
        }
        return userDao.getUserTest(id);
    }
}
