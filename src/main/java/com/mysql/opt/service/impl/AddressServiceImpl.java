package com.mysql.opt.service.impl;

import com.mysql.opt.dao.AddressDao;
import com.mysql.opt.entity.Address;
import com.mysql.opt.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("addressService")
public class AddressServiceImpl implements AddressService {

    @Resource
    private AddressDao addressDao;


    @Override
    public boolean addAddress(List<Address> addresses) {
        return addressDao.addAddresses(addresses);
    }
}
