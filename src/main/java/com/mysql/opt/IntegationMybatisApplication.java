package com.mysql.opt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.mysql.opt.dao")
public class
IntegationMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegationMybatisApplication.class, args);
    }
}