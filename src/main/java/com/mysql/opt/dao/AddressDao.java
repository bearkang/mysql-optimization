package com.mysql.opt.dao;

import com.mysql.opt.entity.Address;

import java.util.List;

public interface AddressDao {

    boolean addAddresses(List<Address> addresses);
}
