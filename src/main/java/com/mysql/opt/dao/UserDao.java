package com.mysql.opt.dao;

import com.mysql.opt.entity.User;
import com.mysql.opt.entity.UserTest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    List<User> getUserList();

    List<User> searchList(@Param("keyword") String keyword);

    boolean addUsers(List<User> users);

    Integer updateById(User user);

    List<UserTest> getUserTest(@Param("id") Integer id);
}